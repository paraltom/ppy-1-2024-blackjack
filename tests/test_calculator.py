import pytest
import sys
from os import path

path_to_src = path.join(sys.path[0], '..', 'src')
sys.path.append(path_to_src)
# for a more intuitive solution look at: 
# https://docs.pytest.org/en/7.1.x/explanation/goodpractices.html

from calculator import add, subtract, multiply, divide

def test_add():
    assert add(2, 3) == 5
    assert add(-1, 1) == 0
    assert add(0, 0) == 0

def test_subtract():
    assert subtract(5, 3) == 2
    assert subtract(-1, 1) == -2
    assert subtract(0, 0) == 0

def test_multiply():
    assert multiply(2, 3) == 6
    assert multiply(-1, 1) == -1
    assert multiply(0, 5) == 0

def test_divide():
    assert divide(6, 3) == 2
    assert divide(-6, 3) == -2
    assert divide(0, 5) == 0
    with pytest.raises(ValueError):
        divide(6, 0)
