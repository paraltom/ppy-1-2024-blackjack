# Bike Sales Dashboard

## Project overview

### Motivation
This project aims to provide interactive dashboard to help users find trends from the bike sales dataset. This can be helpful for businesses in enhancing their sales strategies. The interactive dashboard is created with Streamlit, allowing users to easily navigate through the output.

### Output
The output of this project is an interactive dashboard created with Streamlit, which features:
- Visualize bike sales data using plots.
- View statistical summaries for numerical columns.
- View short interpretation for each column.
- Filter the data based on columns.

Users can interact with the data by filtering and selecting relevant inputs to their analysis.

### Data Source

The dataset used in this project is downloaded from: [Bike Sales Dataset](https://www.kaggle.com/datasets/ahmedmohamedibrahim1/bike-sales-dataset/data).
The bike_buyers sheet contains information about individuals including:

- Demographic Information: Martial Status, Gender, Income, Number of Children, Education, Occupation, Home Ownership, Number of Cars, Commute Distance, Region, Age and Age Bracket.
- Purchase Information: Whether the individual purchased a bike.

## Dependencies

To run the project, you need to install the following dependencies:

- Python 3.7+
- Streamlit
- Pandas
- Matplotlib
- Seaborn

## Classes and Methods
### DataLoader
- ``load_data(file_path, sheet)``: Load data from an Excel file into a DataFrame.

### DataProcessor
- ``clean_data()``: Clean the DataFrame by dropping rows with missing values and removing the unused 'ID' column.

### DataVisualizer
- ``plot_histogram(column)``: Create and display a histogram or count plot for the given column.
- ``calculate_statistics(column, statistic)``: Compute and return a statistic (Median, Mean, Mode, Variance) for a numeric column.
- ``interpret_column(column)``: Provide a short interpretation text for the specified column.

### PageCreator
- ``load_and_clean_data()``: Load and clean data from an Excel file. Initialize the data processing and visualization classes.
- ``apply_filters()``: Apply filters based on selected categories from the sidebar.
- ``display_data()``: Display the filtered DataFrame and visualizations for the selected columns.
- ``run()``: Starts the app and puts it all together.


## How to run the project

1. Clone the repository and navigate to the project directory.
2. Install all the required dependencies.
3. Run the Streamlit application:
``streamlit run BikeSalesAnalysis.py``

## Conclusion

The project provides interactive way to explore bike sales data, allowing users to make informed business decisions. 
